<?php
namespace app\mini_program_app\controller;

use think\Log;

/**
 * Class OitMiniPro
 * oit小程序
 * @package app\mini_program_app\controller
 */
class Oit {
    /**
     * @return string
     */
    public function index() {

        return json(['a' => 'abc', 'b' => 'bcd']);
    }

    /**
     * 微信小程序提交 code 与微信服务器交换openid 与 session_key
     * @return \think\response\Json
     */
    public function login() {

        return json(['3rd_session' => '3rd_session']);
    }

}
