<?php
namespace app\common\model\eba;

use app\common\model\ebs\EbsVr;
use app\common\model\mio\MioAccountIo;
use app\common\model\OitBase;
use think\Db;
use think\Log;

/**
 * 客户
 * Class Eba
 * @package app\eba\model
 */
class Eba extends OitBase {
    public $table = 'eba';
    public $pk = 'eba_id';
    public $pk_name = 'eba_name';
    protected $resultSetType = 'collection'; // 以数组返回

    public $owner_fmt_id = 'eba';
    public $fmt_id = 'eba';

    public $fmt_field_list = [
        ['field' => 'eba_name', 'width' => 80, 'title' => '客户名称'],
        ['field' => 'eba_id', 'width' => 65, 'title' => '客户编号'],
        ['field' => 'linkman', 'width' => 80, 'title' => '联系人'],
        ['field' => 'office_no', 'width' => 30, 'title' => '办公电话'],
        ['field' => 'mobile_no', 'width' => 50, 'title' => '移动电话'],
    ];

    public $field_dict_need = [
        'eba_grade','sex','eba_state', 'eba_service'
    ];

    public $field_dict_def = [
        [
            'field_id' => 'eba_grade',
            'dict_id' => 'eba_grade',
            'r_field_id' => 'eba_grade'
        ],
        [
            'field_id' => 'gender',
            'dict_id' => 'sex',
            'r_field_id' => 'gender'
        ],
        [
            'field_id' => 'state',
            'dict_id' => 'eba_state',
            'r_field_id' => 'state'
        ],

        [
            'field_id' => 'service_id',
            'dict_id' => 'eba_service',
            'r_field_id' => 'service_id'
        ],
    ];


    /**
     * 检查是否能删除某条客户信息
     * 1 有业务单据使用就不能删除
     * 2 有收支单据使用就不能删除
     * @param $eba_id
     * @return mixed
     */
    public function remove_check($eba_id = null) {
        if($eba_id == null){
            return [
                'result' => false,
                'info' => lang('请传递参数') . ' $eba_id'
            ];
        }
        $ebs_vr = new EbsVr();
        $where['eba_id'] = $eba_id;
        $result = $ebs_vr->field('voucher_id')->where($where)->select()->toArray();
        if(!empty($result)){
            return [
                'result' => false,
                'info' => lang('已有业务单据使用了该客户')
            ];
        }
        $mio_account_io = new MioAccountIo();
        $result = $mio_account_io->field('voucher_id')->where($where)->select()->toArray();
        if(!empty($result)){
            return [
                'result' => false,
                'info' => lang('已有收支单据使用了该客户')
            ];
        }
        return [
            'result' => true,
            'info' => lang('没有业务单据与收支单据使用该客户')
        ];
    }


}
