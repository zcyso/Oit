<?php
/**
 * Created by PhpStorm.
 * User: Yang
 * Date: 2017-11-21
 * Time: 15:35
 */

namespace app\common\logic;

use app\common\api\Para;
use app\common\model\eba\Eba;
use app\common\model\eba\EbaGroupMember;
use app\common\model\eba\EbaGroupShare;
use app\common\model\eba\EbaService;
use app\common\model\mup\MupUserBo;
use think\Db;
use think\Log;

/**
 * Class Eba
 * @package app\common\logic
 */
class EbaLogic {

    /**
     * 返回用户权限内的服务区域
     * @param string $type
     * @param null   $user_id
     * @return array|false|\PDOStatement|string|\think\Collection
     */
    public static function user_eba_service($type = 'user', $user_id = null) {
        $eba_service = new EbaService();
        $field = ['service_id', 'service_name', 'parent_service_id'];
        switch ($type) {
            case 'user':
                if ($user_id == null) {
                    $user_id = session('user_id');
                }
                $where['user_id'] = $user_id;

                $bo_val = Para::user_bo_val_now($user_id, 'eba_service');
                if (empty($bo_val)) {
                    $where['service_id'] = ['in', $bo_val];
                }
                // 不使用sql查询，直接从缓存中获取，再编辑逻辑处理数组
                $service_list = $eba_service->where(['service_id' => ['in', $bo_val]])->field($field)->select()->toArray();
                break;
            default:
                $service_list = $eba_service->field($field)->select()->toArray();

        }
        return $service_list;
    }

    /**
     * 获得客户列表
     * @param $type
     * @param $field
     * @return array
     */
    public static function get_eba_list($type, $field) {
        $eba = new Eba();
        $emp_id = session('emp_id');
        $user_id = session('user_id');

        $result = [];
        switch ($type) {
            case 'all':
                $result = $eba->field($field)->select()->toArray();
                break;
            case 'emp':
                $result = $eba->where(['emp_id' => $emp_id])->field($field)->select()->toArray();
                break;
            case 'eba_service':
                $user_eba_service = MupUserBo::all(['user_id' => $user_id, 'bo_id' => 'eba_service'])->toArray();
                if (!empty($user_eba_service)) {
                    $result = $eba->where(['service_id' => ['in', array_column($user_eba_service, 'val')]])->field($field)->select()->toArray();
                }
                break;
            case 'share':
                $share_eba_group = EbaGroupShare::all(['user_id' => $user_id])->toArray();
                if (!empty($share_eba_group)) {
                    $eba_groups = array_column($share_eba_group, 'group_id');
                    $eba_ids = EbaGroupMember::all(['group_id' => ['in', $eba_groups]])->toArray();
                    if (!empty($eba_ids)) {
                        $eba_id_val = array_column($eba_ids, 'eba_id');
                        $result = $eba->where(['eba_id' => ['in', $eba_id_val]])->field($field)->select()->toArray();
                    }
                }
                break;
        }
        return $result;
    }

}